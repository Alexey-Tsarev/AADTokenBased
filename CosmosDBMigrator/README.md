# CosmosDBMigrator

Java demo app, which uses Azure managed identity and mongobee (MongoDB data migration tool for Java).

## Build
````
mvn compile assembly:single
````
## Example
````
COSMOS_ID=<Azure Cosmos Id link> java -jar target/CosmosDBMigrator-1.0-SNAPSHOT-jar-with-dependencies.jar
````
or
````
MONGO_HOST=<Mongo/Cosmos host> MONGO_DB=<Mongo/Cosmos DB> MONGO_USER=<Mongo/Cosmos user> MONGO_PASSWORD=Mongo/Cosmos password> java -jar target/CosmosDBMigrator-1.0-SNAPSHOT-jar-with-dependencies.jar
````

## Result
````
globaldb:PRIMARY> db.mycollection.find()
{ "_id" : ObjectId("5f57571c27e17363463fdfd0"), "testName" : "example", "test" : "1" }
{ "_id" : ObjectId("5f57571e27e17363463fdfd2"), "test" : "1" }
````

Throughput (RU/s) for "mycollection" is 700.
