import com.github.mongobee.Mongobee;
import com.microsoft.azure.AzureEnvironment;
import com.microsoft.azure.credentials.MSICredentials;
import com.microsoft.azure.management.cosmosdb.DatabaseAccountListConnectionStringsResult;
import com.microsoft.azure.management.cosmosdb.DatabaseAccountListKeysResult;
import com.microsoft.rest.LogLevel;

import com.microsoft.azure.management.Azure;
import com.microsoft.azure.management.cosmosdb.CosmosDBAccount;


/*
Required environment variables:
  COSMOS_ID

OR

Required environment variables:
  MONGO_HOST
  MONGO_DB
  MONGO_USER

Optional environment variables:
  MONGO_PORT
  MONGO_PASSWORD
 */

public class CosmosDBMigrator {
    public static void main(String[] args) throws Exception {

        System.out.println("Found arguments number: " + args.length);

        for (int i = 0; i < args.length; i++)
            System.out.println("Argument " + i + ": " + args[i]);

        System.out.println();

        String mongoDB = System.getenv("MONGO_DB");
        if (mongoDB == null) {
            System.err.println("Fatal: required env variable not found: MONGO_DB");
            System.exit(1);
        }

        String mongoURI = "";

        String cosmosId = System.getenv("COSMOS_ID");
        if (cosmosId != null) {
            // MSI Auth
            MSICredentials credentials = new MSICredentials(AzureEnvironment.AZURE);
            Azure azure = null;

            try {
                System.out.println("Start to build Azure credentials");
                azure = Azure
                        .configure()
                        .withLogLevel(LogLevel.BASIC)
                        .authenticate(credentials)
                        .withDefaultSubscription();
            } catch (Exception e) {
                System.err.println("Got exception: " + e.getMessage());
                e.printStackTrace();
            }
            // End MSI Auth

            CosmosDBAccount cosmosDBAccount = azure.cosmosDBAccounts().getById(cosmosId);
            System.out.println("CosmosDBAccount: " + cosmosDBAccount);

            DatabaseAccountListConnectionStringsResult databaseAccountListConnectionStringsResult = cosmosDBAccount.listConnectionStrings();
//            DatabaseAccountListKeysResult keys = cosmosDBAccount.listKeys();
//            System.out.println(keys);

            mongoURI = databaseAccountListConnectionStringsResult.connectionStrings().get(0).connectionString();

//            MongoClientURI uri = new MongoClientURI(databaseAccountListConnectionStringsResult.connectionStrings().get(0).connectionString());
//            MongoClient mongoClient = null;
        } else {
            String mongoHost = System.getenv("MONGO_HOST");
            if (mongoHost == null) {
                System.err.println("Fatal: required env variable not found: MONGO_HOST");
                System.exit(1);
            }

            String mongoPort = System.getenv("MONGO_PORT");
            if (mongoPort == null) {
                mongoPort = "10255";
            }

            String mongoOpts = System.getenv("MONGO_OPTS");
            if (mongoOpts == null) {
                mongoOpts = "ssl=true";
            }

            String mongoCollection = System.getenv("MONGO_COLLECTION");
            if (mongoCollection == null) mongoCollection = "";

            String mongoUser = System.getenv("MONGO_USER");
            if (mongoUser == null) {
                System.err.println("Fatal: required env variable not found: MONGO_USER");
                System.exit(2);
            }

            String mongoPassword = System.getenv("MONGO_PASSWORD");
            if (mongoPassword == null) {
                System.out.println("Env variable not found: MONGO_PASSWORD. Trying get it via Azure managed identity");
                System.out.println();
            }

            System.out.println("Mongo host: " + mongoHost);
            System.out.println("Mongo port: " + mongoPort);
            System.out.println("Mongo options: " + mongoOpts);
            System.out.println("Mongo DB: " + mongoDB);
            System.out.println("Mongo user: " + mongoUser);
            System.out.println("Mongo password: " + mongoPassword);
            mongoURI = "mongodb://" + mongoUser + ":" + mongoPassword + "@" + mongoHost + ":" + mongoPort + "/" + mongoCollection + "?" + mongoOpts;
        }

        System.out.println("Mongo connection string: " + mongoURI);
        System.out.println();


        try {
            Mongobee runner = new Mongobee(mongoURI);
//        Mongobee runner = new Mongobee("mongodb://cosmos-ib-ue1-test-atsarev-lkna:TbJawiWmGF7uqWg2rLcPsydvytr8brjOPUgsYBnJBEwKwGhzANPVga32Y89lq9cG06UbXOGf0XX3DQ5MCYz6ow==@cosmos-ib-ue1-test-atsarev-lkna.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmos-ib-ue1-test-atsarev-lkna@");

//        mongodb://cosmos-ib-ue1-test-atsarev-lkna:TbJawiWmGF7uqWg2rLcPsydvytr8brjOPUgsYBnJBEwKwGhzANPVga32Y89lq9cG06UbXOGf0XX3DQ5MCYz6ow==@cosmos-ib-ue1-test-atsarev-lkna.mongo.cosmos.azure.com:10255/?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@cosmos-ib-ue1-test-atsarev-lkna@");
//        mongodb://cosmos-ib-ue1-test-atsarev-lkna:TbJawiWmGF7uqWg2rLcPsydvytr8brjOPUgsYBnJBEwKwGhzANPVga32Y89lq9cG06UbXOGf0XX3DQ5MCYz6ow==@cosmos-ib-ue1-test-atsarev-lkna.mongo.cosmos.azure.com/?ssl=true
            runner.setDbName(mongoDB);
            runner.setChangeLogsScanPackage("changelogs"); // package to scan for changesets

            // Starts migration changesets
            runner.execute();
        } catch (Exception e) {
            System.err.println("Got exception: " + e);
            e.printStackTrace();
        }

        //        String accessToken = "";
//
//        if (args.length > 3) {
//            System.out.println("Take token from command line");
//
//            accessToken = args[3];
//        } else {
//            System.out.println("Trying to take a token");
//
//            MSICredentials credsProvider = MSICredentials.getMSICredentials("https://database.windows.net/");
//            MSIToken token = credsProvider.getToken(null);
//            accessToken = token.accessToken();
//        }
//
//        System.out.println("Token:");
//        System.out.println(accessToken);
//        System.out.println();
//
//        SQLServerDataSource ds = new SQLServerDataSource();
//
//        ds.setServerName(serverName);
//        ds.setDatabaseName(databaseName);
//        ds.setAccessToken(accessToken);
//
//        try {
//            System.out.println("getConnection");
//            Connection connection = ds.getConnection();
//
//            System.out.println("createStatement");
//            Statement stmt = connection.createStatement();
//
//            System.out.println("Running query: " + query);
//            ResultSet rs = stmt.executeQuery(query);
//
//            int resultLine = 1;
//
//            while (rs.next()) {
//                ResultSetMetaData rsmd = rs.getMetaData();
//                int columnCount = rsmd.getColumnCount();
//
//                System.out.print("Result line " + resultLine + ": ");
//
//                for (int i = 1; i <= columnCount; i++)
//                    System.out.print(rs.getString(i) + " ");
//
//                System.out.println();
//                resultLine++;
//            }
//        } catch (SQLServerException e) {
//            System.err.print("Exception: " + e);
//        }
    }
}
