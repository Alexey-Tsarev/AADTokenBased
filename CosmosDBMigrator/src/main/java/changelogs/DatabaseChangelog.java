package changelogs;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.BsonDocument;
import org.bson.BsonJavaScript;
import org.bson.Document;
import org.jongo.Jongo;
import org.springframework.data.mongodb.core.MongoTemplate;

@ChangeLog
public class DatabaseChangelog {
    String defautAuthor = "Alexey Tsarev";

    @ChangeSet(order = "001", id = "changeWithoutArgs", author = "Alexey Tsarev 3")
    public void change1() {
        // method without arguments can do some non-db changes
        System.out.println("001");
    }

    @ChangeSet(order = "002", id = "changeWithMongoDatabase", author = "Alexey Tsarev 3")
    public void change2(MongoDatabase db) {
        System.out.println("002");

        // type: com.mongodb.client.MongoDatabase : original MongoDB driver v. 3.x, operations allowed by driver are possible
        // example:
        MongoCollection<Document> mycollection = db.getCollection("mycollection");
        Document doc = new Document("testName", "example").append("test", "1");
        mycollection.insertOne(doc);
    }

    @ChangeSet(order = "003", id = "changeWithDb", author = "Alexey Tsarev 3")
    public void change3(DB db) {
        System.out.println("003");

        // This is deprecated in mongo-java-driver 3.x, use MongoDatabase instead
        // type: com.mongodb.DB : original MongoDB driver v. 2.x, operations allowed by driver are possible
        // example:
        DBCollection mycollection = db.getCollection("mycollection");
        BasicDBObject doc = new BasicDBObject().append("test", "1");
        mycollection.insert(doc);
    }

    @ChangeSet(order = "004", id = "changeMycollectionThroughput", author = "Alexey Tsarev 3")
    public void change4(MongoDatabase db) {
        System.out.println("004");

        db.runCommand(new Document("customAction", "UpdateCollection")
                .append("collection", "mycollection")
                .append("offerThroughput", 700));
    }

    @ChangeSet(order = "005", id = "addJSFuncToMycollection", author = "Alexey Tsarev 3")
    public void change5(MongoDatabase db) {
        System.out.println("005");

        BsonDocument echoFunction = new BsonDocument("value",
                new BsonJavaScript("function(x1) { return x1; }"));

        BsonDocument myAddFunction = new BsonDocument("value",
                new BsonJavaScript("function (x, y) { return x + y; }"));

        db.getCollection("system.js").updateOne(
                new Document("_id", "echoFunction"),
                new Document("$set", echoFunction),
                new UpdateOptions().upsert(true));

        db.getCollection("system.js").updateOne(
                new Document("_id", "myAddFunction"),
                new Document("$set", myAddFunction),
                new UpdateOptions().upsert(true));

        /*
        Below commented code failed with the:
        > com.github.mongobee.exception.MongobeeException: Command failed with error 59: 'Command $eval not found.'
        https://docs.microsoft.com/en-US/azure/cosmos-db/mongodb-feature-support#unsupported-operators
        > The $where and the $eval operators are not supported by Azure Cosmos DB.

        https://docs.mongodb.com/manual/reference/method/db.eval/
        > Starting in version 4.2, MongoDB removes the eval command.
        */
        /*
        db.runCommand(new Document("$eval", "db.loadServerScripts()"));

        Document doc1 = db.runCommand(new Document("$eval", "echoFunction(3)"));
        System.out.println(doc1.toJson());

        Document doc2 = db.runCommand(new Document("$eval", "myAddFunction(5, 6)"));
        System.out.println(doc2);
        */
    }

    @ChangeSet(order = "998", id = "changeWithJongo", author = "Alexey Tsarev 3")
    public void change998(Jongo jongo) {
        System.out.println("998");

        // type: org.jongo.Jongo : Jongo driver can be used, used for simpler notation
        // example:
//        MongoCollection mycollection = (MongoCollection) jongo.getCollection("mycollection");
//        mycollection.insertOne("{test : 1}");
    }

    @ChangeSet(order = "999", id = "changeWithSpringDataTemplate", author = "Alexey Tsarev")
    public void change999(MongoTemplate mongoTemplate) {
        System.out.println("999");

        // type: org.springframework.data.mongodb.core.MongoTemplate
        // Spring Data integration allows using MongoTemplate in the ChangeSet
        // example:
//        mongoTemplate.save(myEntity);
    }
}
