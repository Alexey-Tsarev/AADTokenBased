import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import com.microsoft.azure.msiAuthTokenProvider.*;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

public class AADTokenSQL {
    public static void main(String[] args) throws Exception {

        System.out.println("Arguments number: " + args.length);

        for (int i = 0; i < args.length; i++)
            System.out.println("Argument " + i + ": " + args[i]);

        System.out.println();

        if (args.length < 3) {
            System.err.println("Number of cmd arguments less 3");
            System.exit(1);
        }

        String serverName = args[0];
        String databaseName = args[1];
        String query = args[2];

        System.out.println("Server Name: " + serverName);
        System.out.println("Database Name: " + databaseName);
        System.out.println("Query: " + query);
        System.out.println();

        String accessToken = "";

        if (args.length > 3) {
            System.out.println("Take token from command line");

            accessToken = args[3];
        } else {
            System.out.println("Trying to take a token");

            MSICredentials credsProvider = MSICredentials.getMSICredentials("https://database.windows.net/");
            MSIToken token = credsProvider.getToken(null);
            accessToken = token.accessToken();
        }

        System.out.println("Token:");
        System.out.println(accessToken);
        System.out.println();

        SQLServerDataSource ds = new SQLServerDataSource();

        ds.setServerName(serverName);
        ds.setDatabaseName(databaseName);
        ds.setAccessToken(accessToken);

        try {
            System.out.println("getConnection");
            Connection connection = ds.getConnection();

            System.out.println("createStatement");
            Statement stmt = connection.createStatement();

            System.out.println("Running query: " + query);
            ResultSet rs = stmt.executeQuery(query);

            int resultLine = 1;

            while (rs.next()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();

                System.out.print("Result line " + resultLine + ": ");

                for (int i = 1; i <= columnCount; i++)
                    System.out.print(rs.getString(i) + " ");

                System.out.println();
                resultLine++;
            }
        } catch (SQLServerException e) {
            System.err.print("Exception: " + e);
        }
    }
}
