# AADTokenSQL

Azure AD token for SQL: Java demo app

Build jar:
````
mvn compile assembly:single
````

Setup desired user identity in Azure and run:
````
java -jar target/AADTokenSQL-1.0-SNAPSHOT-jar-with-dependencies.jar <Server Name> <Database Name> <Query>
````

or get a token manually via:
````
sudo dnf install -y jq
curl --silent -H "Metadata: true" "http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https://database.windows.net/" | jq -r .access_token
````

and run:
````
java -jar target/AADTokenSQL-1.0-SNAPSHOT-jar-with-dependencies.jar <Server Name> <Database Name> <Query> <Token>
````

As an example, run the following queries in a sequence:
````
CREATE TABLE test (a INT, b INT)
INSERT INTO test VALUES (1, 2)
INSERT INTO test VALUES (2, 3)
INSERT INTO test VALUES (3, 4)
INSERT INTO test VALUES (4, 5)
SELECT * FROM test
````

And the result is:
````
Query: SELECT * FROM test

result line 1: 1 2
result line 2: 2 3
result line 3: 3 4
result line 4: 4 5
````
